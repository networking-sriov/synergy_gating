#!/bin/bash
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#   setup_trex.sh of /kernel/networking/openvswitch/perf
#   Author: Qijun Ding <qding@redhat.com>
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#   Copyright (c) 2013 Red Hat, Inc.
#
#   This copyrighted material is made available to anyone wishing
#   to use, modify, copy, or redistribute it subject to the terms
#   and conditions of the GNU General Public License version 2.
#
#   This program is distributed in the hope that it will be
#   useful, but WITHOUT ANY WARRANTY; without even the implied
#   warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE. See the GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public
#   License along with this program; if not, write to the Free
#   Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
#   Boston, MA 02110-1301, USA.
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

pcis=()

trex_url="http://netqe-infra01.knqe.lab.eng.bos.redhat.com/tools/v2.87.tar.gz"
trex_target_dir="/opt/trex"
trafficgen_url="http://netqe-infra01.knqe.lab.eng.bos.redhat.com/tools/trafficgen-20210913.tgz"
trafficgen_target_dir="/opt/trafficgen"
vlan=0

usage()
{
	cat <<-EOF
	$0 [options] pci1 [pci2 ...]
	options:
	-h|--help                       show this message
	--trex-url                      url to download trex
	--trex-target-dir               target directory to save trex
	--trafficgen-url                url to download traffic genertor
	--trafficgen-dir                target directory to save traffic genertor
	--without-install-trex          skip installing trex
	--without-install-trafficgen    skip installing traffic generator
	--without-start-trex            skip starting trex
	--vlan                          0 means no vlan
	--cores-include					White list of cores to use.
	EOF
}

install_trex()
{
	rpm -q driverctl &>/dev/null || yum -y install driverctl
	rpm -q libibverbs &>/dev/null || yum -y install libibverbs
	
	if pgrep t-rex &>/dev/null
	then
		pkill t-rex
		sleep 5
	fi

	mkdir -p $trex_target_dir
	chmod 777 $trex_target_dir

	pushd $trex_target_dir &>/dev/null
	if ! test -f $(basename $trex_url)
	then
		wget -nv -N -P ${trex_target_dir} ${trex_url} ||
		{ echo FAIL to download ${trex_url}!; exit 1; }
	fi
	mkdir -p ${trex_target_dir}/current
	chmod 777 ${trex_target_dir}/current
	rm -rf ${trex_target_dir}/current/*
	tar xzf ${trex_target_dir}/$(basename ${trex_url}) --strip 1 -C ${trex_target_dir}/current/ || \
	{ echo FAIL to decompress $(basename ${trex_url})!; exit 1; }
	popd &>/dev/null

	return 0
}

start_trex()
{
	if pgrep t-rex &>/dev/null
	then
		pkill -f t-rex
		sleep 5
	fi

	pushd $trex_target_dir &>/dev/null
	pushd current &>/dev/null
	rm -rf /etc/trex_cfg.yaml
	driverctl list-overrides | awk '{ system("driverctl unset-override "$1) }'
	./dpdk_setup_ports.py -L
	if [[ -n "${cores_include}" ]]; then
		./dpdk_setup_ports.py -c ${pcis[@]} --ci ${cores_include} -o /etc/trex_cfg.yaml --force-macs
	else
		./dpdk_setup_ports.py -c ${pcis[@]} -o /etc/trex_cfg.yaml --force-macs
	fi
	if ! test -f /etc/trex_cfg.yaml
	then
		echo FAIL to create /etc/trex_cfg.yaml!
		exit 1
	fi
	cat >> /etc/trex_cfg.yaml <<-EOF
	  memory:
	      mbuf_64     : 81920
	      mbuf_128    : 51200
	      mbuf_256    : 38400
	      mbuf_512    : 38400
	      mbuf_1024   : 38400
	      mbuf_2048   : 38400
	      mbuf_4096   : 38400
	      mbuf_9k     : 38400
	      global      : 5120
	EOF

	# replace mac addresses
	#mac1=$(grep -o "\([0-9a-f]\{2\}:\)\{5\}[0-9a-f]\{2\}" /etc/trex_cfg.yaml | sed -n '1p')
	#mac0=$(grep -o "\([0-9a-f]\{2\}:\)\{5\}[0-9a-f]\{2\}" /etc/trex_cfg.yaml | sed -n '2p')
	#sed -i "s/$mac1/00:00:00:00:00:02/g" /etc/trex_cfg.yaml
	#sed -i "s/$mac0/00:00:00:00:00:01/g" /etc/trex_cfg.yaml

	#if ./dpdk_nic_bind.py -s | grep drv=vfio-pci &>/dev/null
	#then
	#	./dpdk_nic_bind.py -s | grep drv=vfio-pci | awk -F'[ =]' '{ system("./dpdk_nic_bind.py -b "$NF" "$1) }'
	#	sleep 2
	#fi
	popd &>/dev/null
	popd &>/dev/null
	cat /etc/trex_cfg.yaml
	# For Mellanox card
	# Add sleep time to avoid print "NO-CARRIER" link status
	for pci in ${pcis[@]}
	do
		ls /sys/bus/pci/devices/${pci}/net &>/dev/null || continue
		local interfaces=$(ls /sys/bus/pci/devices/${pci}/net)
		local iface=""
		for iface in $interfaces
		do
			if [[ -n "$iface" ]] && ip link show $iface &>/dev/null
			then
				ethtool --show-priv-flags $iface | grep disable-fw-lldp && ethtool --set-priv-flags $iface disable-fw-lldp on
				sleep 1
				nmcli -v &>/dev/null && nmcli dev set $iface managed no
				sleep 1
				ip link set $iface up
				sleep 1
				ip link set mtu 9120 dev $iface
				sleep 1
				ethtool -A ${iface} rx off tx off
				sleep 1
				ip addr flush $iface
				sleep 1
				ip -d addr show $iface
			fi
		done
	done
	# when starting T-Rex server, this should be executed automatically,
	# but sometimes it just needs more time so that it will fail
	modprobe ib_uverbs
	sleep 2

	rpm -q jq &>/dev/null || yum -y install jq
	pushd $trex_target_dir/current &>/dev/null
	ds=$(./dpdk_nic_bind.py --status --json | jq ".\"${pcis[0]}\".\"Driver_str\"")
	ms=$(./dpdk_nic_bind.py --status --json | jq ".\"${pcis[0]}\".\"Module_str\"")

	if [[ "$ds" =~ mlx* ]]
	then
		echo "Test nic belong Mallenox, do nothing"
	else
		lsmod | grep vfio_pci || modprobe vfio_pci
		sleep 5
		local dev=''
		for dev in ${pcis[@]}
		do
			# https://github.com/cisco-system-traffic-generator/trex-core/issues/413
			# for all the devices in the same slot
			local d=$(echo $dev | awk -F[:.] '{print $1}')
			local b=$(echo $dev | awk -F[:.] '{print $2}')
			local s=$(echo $dev | awk -F[:.] '{print $3}')
			local f=$(echo $dev | awk -F[:.] '{print $4}')
			local dev_list=$(find /sys/bus/pci/devices/ -name $d:$b:$s\.* | sed 's|/sys/bus/pci/devices/||g')
			for i in $dev_list
			do
				driverctl set-override ${i} vfio-pci
				#./dpdk_nic_bind.py -b vfio-pci ${i}
			done
		done
		sleep 5
		driverctl list-overrides
	fi

	n_cpus=$(sed -n -e 's/.*threads: \[\(.*\)\]/\1/p' /etc/trex_cfg.yaml | sed 's/,/\n/g' | wc -l)
	((n_cpus > 31)) && n_cpus=31
	if ([[ "$ds" = '"ixgbe"' ]] || [[ "$ms" = '"ixgbe"' ]]) && ((vlan))
	then
		nohup ./t-rex-64 --no-ofed-check --vlan -c ${n_cpus} -i 1>/tmp/trex_perf.log &
	else
		nohup ./t-rex-64 --no-ofed-check -c ${n_cpus} -i 1>/tmp/trex_perf.log &
	fi
	sleep 30
	popd &>/dev/null

	ps -ef | grep t-rex
	if ! pgrep t-rex &>/dev/null
	then
		echo FAIL to start T-Rex server!
		exit 1
	fi

	return 0
}

install_trafficgen()
{
	mkdir -p "${trafficgen_target_dir}"
	chmod 777 "${trafficgen_target_dir}"
	if ! test -f /tmp/$(basename $trafficgen_url)
	then
		wget -nv -N -P /tmp/ ${trafficgen_url} || \
		{ echo FAIL to download ${trafficgen_url}!; exit 1; }
	fi
	tar xf /tmp/$(basename ${trafficgen_url}) --strip 1 -C ${trafficgen_target_dir} || \
	{ echo FAIL to decompress $(basename ${trafficgen_url})!; exit 1; }

	return 0
}

options=$(getopt -o h --long help,trex-url:,trex-target-dir:,trafficgen-url:,trafficgen-dir:,without-start-trex,without-install-trafficgen,without-install-trex,vlan:,cores-include:, -- "$@")
[[ $? -eq 0 ]] || {
	echo "Incorrect options provided"
	exit 1
}
eval set -- "$options"
while true
do
	case "$1" in
	-h | --help)
		usage
		exit 0
		;;
	--trex-url)
		shift
		trex_url=$1
		;;
	--trex-target-dir)
		shift
		trex_target_dir=$1
		;;
	--trafficgen-url)
		shift
		trafficgen_url=$1
		;;
	--trafficgen-target-dir)
		shift
		trafficgen_target_dir=$1
		;;
	--without-start-trex)
		without_start_trex="yes"
		;;
	--without-install-trafficgen)
		without_install_trafficgen="yes"
		;;
	--without-install-trex)
		without_install_trex="yes"
		;;
	--vlan)
		shift
		vlan="$1"
		;;
	--cores-include)
		shift
		cores_include="$1"
		;;
	--)
		shift
		break
		;;
	esac
	shift
done

echo trex_url=$trex_url
echo trex_target_dir=$trex_target_dir
echo trafficgen_url=$trafficgen_url
echo trafficgen_target_dir=$trafficgen_target_dir
echo cores-include=$cores_include

which python &>/dev/null || which python3 &>/dev/null || yum -y install python3
which python &>/dev/null || ln -s $(which python3) /usr/bin/python
rpm -q pciutils &>/dev/null || yum install -y pciutils

[[ "$without_install_trex" == "yes" ]] || install_trex
[[ "$without_install_trafficgen" == "yes" ]] || install_trafficgen

if [[ "$without_start_trex" != "yes" ]]
then
	pcis=($@)

	echo pcis=${pcis[@]}
	echo vlan=${vlan}
	
	[[ -z "${pcis[*]}" ]] && { echo "FAIL: NICs must be specified to run T-Rex !"; exit 1; }
	
	for nic in ${pcis[@]}
	do
		if ! [[ $nic =~ [0-9a-fA-F]{4}:[0-9a-fA-F]{2}:[0-9a-fA-F]{2}\.[0-9a-fA-F] ]] ||
			! [[ $nic =~ [0-9a-fA-F]{2}:[0-9a-fA-F]{2}\.[0-9a-fA-F] ]]
		then
			echo "FAIL: invalid PCI address $nic!"
			exit 1
		fi
	done
	
	start_trex
fi
